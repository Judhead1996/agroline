import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { LoginService } from '../services/auth/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  result : any;
  constructor(private authService: LoginService, private router:Router, private alertCtrl: AlertController,
    private toastController: ToastController, public loadingCtrl: LoadingController) { }

  ngOnInit() {
  }

  async onLogin(user){
    var loader = await this.loadingCtrl.create({
        message: "Please Wait...",
        duration: 2000
    });
    await loader.present();
    this.authService.login(user).subscribe(data => {
      this.result = JSON.parse(JSON.stringify(data));
      console.log(this.result);
      if(this.result.isLogged == 1){
        localStorage.setItem('code', this.result.code)
        loader.dismiss();
        this.router.navigateByUrl('/home');
        this.showToast('Authentification réussie!');
      }else{
        loader.dismiss();
        this.presentAlert();
      }
    },(error: any) => {
      console.log(error);  
      if(error.status == 400 ){
        //this.alertIdentifiant();
      }else{
        loader.dismiss();
        this.alertInternet();
      }
     
    });
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Erreur',
      message: 'Veuillez vérifier vos identifiants!',
      buttons: ['OK']
    });
  
    await alert.present();
  }

  async alertInternet(){
    const alert = await this.alertCtrl.create({
      header: 'Erreur',
      message: 'Veuillez vérifier la connexion à internet!',
      buttons: ['OK']
    });  
  
    await alert.present();
  }

   // Helper
   async showToast(msg){
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
