import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {rootUrl} from "../../url-api/server-url";


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  login(data){
    return this.http.post(rootUrl+'apitest/utilisateur/connexion.php', data, {})
  }

}
