import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {rootUrl} from "../../url-api/server-url";


@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(private http: HttpClient) { }

  postPriseCommande(data) {
    return this.http.post(rootUrl + "apitest/prc/liste.php", data, {})
  }

}
