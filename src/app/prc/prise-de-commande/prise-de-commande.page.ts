import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { CrudService } from '../../services/api/crud.service';


@Component({
  selector: 'app-prise-de-commande',
  templateUrl: './prise-de-commande.page.html',
  styleUrls: ['./prise-de-commande.page.scss'],
})
export class PriseDeCommandePage implements OnInit {

  result : any;

  constructor(private service:CrudService, public loadingCtrl: LoadingController, private toastController: ToastController,
    private alertCtrl: AlertController, private router : Router) { }

  ngOnInit() {
    this.getPriseCommande();
  }

  async getPriseCommande(){
    let code = localStorage.getItem("code");
    var loader = await this.loadingCtrl.create({
      message: "Please Wait...",
      duration: 2000
   });
  await loader.present();
    this.service.postPriseCommande({
      codeCommerciale: code,
    }).subscribe(data => {
      this.result = data;
      console.log(this.result);
      loader.dismiss();
    });
  } 

  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      header: 'Confirm!',
      message: 'Voulez vous vraiment le supprimer ?',
      buttons: [
        {
          text: 'Non', 
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            
          }
        }, {
          text: 'Oui',
          handler: () => {
            //
          }
        }
      ]
    });
    await alert.present();
  }

  // Helper
  async showToast(msg){
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  addPrc(){
    this.router.navigateByUrl('/add-prc');
  }

}
