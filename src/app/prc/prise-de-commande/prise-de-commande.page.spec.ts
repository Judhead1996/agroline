import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PriseDeCommandePage } from './prise-de-commande.page';

describe('PriseDeCommandePage', () => {
  let component: PriseDeCommandePage;
  let fixture: ComponentFixture<PriseDeCommandePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PriseDeCommandePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PriseDeCommandePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
