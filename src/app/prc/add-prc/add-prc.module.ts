import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddPrcPageRoutingModule } from './add-prc-routing.module';

import { AddPrcPage } from './add-prc.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddPrcPageRoutingModule
  ],
  declarations: [AddPrcPage]
})
export class AddPrcPageModule {}
