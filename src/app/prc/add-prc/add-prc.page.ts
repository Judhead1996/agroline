import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ClientPage } from '../modal/client/client.page';

@Component({
  selector: 'app-add-prc',
  templateUrl: './add-prc.page.html',
  styleUrls: ['./add-prc.page.scss'],
})
export class AddPrcPage implements OnInit {

  dataReturn : string;
  constructor(public modalController: ModalController) { }

  ngOnInit() {
  }

  async clientModal() {
    const modal = await this.modalController.create({
      component: ClientPage
    });
    modal.onWillDismiss().then(dataReturned => {
      this.dataReturn = dataReturned.data;
      //console.log('Receive: ', this.dataReturn)
      if(this.dataReturn){
        
      }
    });
    return await modal.present();
  }

}
