import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddPrcPage } from './add-prc.page';

const routes: Routes = [
  {
    path: '',
    component: AddPrcPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddPrcPageRoutingModule {}
