import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddPrcPage } from './add-prc.page';

describe('AddPrcPage', () => {
  let component: AddPrcPage;
  let fixture: ComponentFixture<AddPrcPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPrcPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddPrcPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
